export const GET_ERRORS = "GET_ERRORS";
export const GET_COURSES = "GET_COURSES";
export const GET_COURSE = "GET_COURSE";
export const UPDATE_COURSE = "UPDATE_COURSE";
