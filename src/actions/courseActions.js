import axios from "axios";
import { GET_ERRORS, GET_COURSES, GET_COURSE, UPDATE_COURSE } from "./types";

export const createCourse = (course, history) => async (dispatch) => {
  try {
    const res = await axios.post("http://localhost:8080/api/courses", course);
    history.push("/dashboard");
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getCourses = () => async (dispatch) => {
  const res = await axios.get("http://localhost:8080/api/courses");
  dispatch({
    type: GET_COURSES,
    payload: res.data,
  });
};

export const getCourse = (id, history) => async (dispatch) => {
  try {
    const res = await axios.get(`http://localhost:8080/api/courses/${id}`);
    dispatch({
      type: GET_COURSE,
      payload: res.data,
    });
  } catch (error) {
    history.push("/dashboard");
  }
};

export const updateCourse = (course, history) => async (dispatch) => {
  try {
    const id = course.id;
    const res = await axios.post(
      `http://localhost:8080/api/courses/${id}`,
      course
    );
    history.push("/dashboard");
    dispatch({
      type: GET_ERRORS,
      payload: {},
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};
