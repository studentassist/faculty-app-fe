import { GET_COURSES, GET_COURSE, UPDATE_COURSE } from "../actions/types";

const initialState = {
  courses: [],
  course: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COURSES:
      return {
        ...state,
        courses: action.payload,
      };
    case GET_COURSE:
      return {
        ...state,
        course: action.payload,
      };
    case UPDATE_COURSE:
      return {
        ...state,
        courses: action.payload,
      };
    default:
      return state;
  }
}
