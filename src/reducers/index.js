import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import courseReducer from "./courseReducer";

export default combineReducers({
  errors: errorReducer,
  course: courseReducer
});
 