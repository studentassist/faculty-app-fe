import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCourse, updateCourse } from "../../actions/courseActions";
import classnames from "classnames";

class UpdateCourse extends Component {
  constructor() {
    super();

    this.state = {
      id: "",
      name: "",
      identifier: "",
      description: "",
      credits: 5,
      semester: 1,
      year: 1,
      teacherId: 1,
      errors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    const {
      id,
      name,
      identifier,
      description,
      credits,
      semester,
      year,
      teacherId,
    } = nextProps.course;

    this.setState({
      id,
      name,
      identifier,
      description,
      credits,
      semester,
      year,
      teacherId,
    });
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getCourse(id, this.props.history);
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const updateCoursed = {
      id: this.state.id,
      name: this.state.name,
      identifier: this.state.identifier,
      description: this.state.description,
      credits: this.state.credits,
      semester: this.state.semester,
      year: this.state.year,
      teacherId: this.state.teacherId,
    };

    this.props.updateCourse(updateCoursed, this.props.history);
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="register">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h5 className="display-4 text-center">Update Course</h5>
              <hr />
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.name,
                    })}
                    placeholder="Course Name"
                    name="name"
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.identifier,
                    })}
                    placeholder="Course Identifier"
                    name="identifier"
                    value={this.state.identifier}
                    onChange={this.onChange}
                    disabled
                  />
                  {errors.identifier && (
                    <div className="invalid-feedback">{errors.identifier}</div>
                  )}
                </div>
                <div className="form-group">
                  <textarea
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.description,
                    })}
                    placeholder="Course Description"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                  {errors.description && (
                    <div className="invalid-feedback">{errors.description}</div>
                  )}
                </div>

                <div className="form-group">
                  <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect01"
                  >
                    Credits
                  </label>
                  <select
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.credits,
                    })}
                    id="inputGroupSelect01"
                    name="credits"
                    value={this.state.credits}
                    onChange={this.onChange}
                  >
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                  {errors.credits && (
                    <div className="invalid-feedback">{errors.credits}</div>
                  )}
                </div>

                <div className="form-group">
                  <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect02"
                  >
                    Semester
                  </label>
                  <select
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.semester,
                    })}
                    id="inputGroupSelect02"
                    name="semester"
                    value={this.state.semester}
                    onChange={this.onChange}
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                  </select>
                  {errors.semester && (
                    <div className="invalid-feedback">{errors.semester}</div>
                  )}
                </div>

                <div className="form-group">
                  <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect03"
                  >
                    Year
                  </label>
                  <select
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.year,
                    })}
                    id="inputGroupSelect03"
                    name="year"
                    value={this.state.year}
                    onChange={this.onChange}
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                  {errors.year && (
                    <div className="invalid-feedback">{errors.year}</div>
                  )}
                </div>

                <input
                  type="submit"
                  className="btn btn-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateCourse.propTypes = {
  getCourse: PropTypes.func.isRequired,
  updateCourse: PropTypes.func.isRequired,
  course: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  course: state.course.course,
  errors: state.errors,
});

export default connect(mapStateToProps, { getCourse, updateCourse })(
  UpdateCourse
);
