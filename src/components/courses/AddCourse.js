import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createCourse } from "../../actions/courseActions";
import classnames from "classnames";

class AddCourse extends Component {
  constructor() {
    super();

    this.state = {
      name: "",
      description: "",
      credits: 5,
      semester: 1,
      year: 1,
      teacherId: 1,
      errors: {},
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const newCourse = {
      name: this.state.name,
      description: this.state.description,
      credits: this.state.credits,
      semester: this.state.semester,
      year: this.state.year,
      teacherId: 1,
    };
    this.props.createCourse(newCourse, this.props.history);
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="register">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h5 className="display-4 text-center">Create Course</h5>
              <hr />
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.name,
                    })}
                    placeholder="Course Name"
                    name="name"
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.identifier,
                    })}
                    placeholder="Course Identifier"
                    name="name"
                    value={this.state.identifier}
                    onChange={this.onChange}
                  />
                  {errors.identifier && (
                    <div className="invalid-feedback">{errors.identifier}</div>
                  )}
                </div>
                <div className="form-group">
                  <textarea
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.description,
                    })}
                    placeholder="Course Description"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                  {errors.description && (
                    <div className="invalid-feedback">{errors.description}</div>
                  )}
                </div>

                <div className="form-group">
                  <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect01"
                  >
                    Credits
                  </label>
                  <select
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.credits,
                    })}
                    id="inputGroupSelect01"
                    name="credits"
                    value={this.state.credits}
                    onChange={this.onChange}
                  >
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                  {errors.credits && (
                    <div className="invalid-feedback">{errors.credits}</div>
                  )}
                </div>

                <div className="form-group">
                  <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect02"
                  >
                    Semester
                  </label>
                  <select
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.semester,
                    })}
                    id="inputGroupSelect02"
                    name="semester"
                    value={this.state.semester}
                    onChange={this.onChange}
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                  </select>
                  {errors.semester && (
                    <div className="invalid-feedback">{errors.semester}</div>
                  )}
                </div>

                <div className="form-group">
                  <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect03"
                  >
                    Year
                  </label>
                  <select
                    className={classnames("form-control form-control-lg ", {
                      "is-invalid": errors.year,
                    })}
                    id="inputGroupSelect03"
                    name="year"
                    value={this.state.year}
                    onChange={this.onChange}
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                  {errors.year && (
                    <div className="invalid-feedback">{errors.year}</div>
                  )}
                </div>

                <input
                  type="submit"
                  className="btn btn-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  errors: state.errors,
});

AddCourse.propTypes = {
  createCourse: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, { createCourse })(AddCourse);
