import React, { Component } from "react";
import CourseItem from "./courses/CourseItem";
import CreateCourseButton from "./courses/CreateCourseButton";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCourses } from "../actions/courseActions";

class Dashboard extends Component {
  componentDidMount() {
    this.props.getCourses();
  }

  render() {
    const { courses } = this.props.course;

    return (
      <div className="courses">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="display-4 text-center">Courses</h1>
              <br />
              <CreateCourseButton />
              <br />
              <hr />
              {courses.map((course) => (
                <CourseItem key={course.id} course={course} />
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  course: PropTypes.object.isRequired,
  getCourses: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  course: state.course,
});

export default connect(mapStateToProps, { getCourses })(Dashboard);
